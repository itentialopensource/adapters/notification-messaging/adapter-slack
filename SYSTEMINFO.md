# Adapter for Slack

Vendor: Slack
Homepage: https://slack.com/

Product: Slack
Product Page: https://slack.com/

## Introduction
We classify Slack into the Notifications domaina as Slack provides an instant messaging solution for communications within an Organization. 

"Scale your processes, ensure company-wide compliance, and maximize tech stack adoption—all with the power of Slack."

## Why Integrate
The Slack adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Slack. With this adapter you have the ability to perform operations with Slack on items such as:

- Message

## Additional Product Documentation
The [Slack API Reference](https://api.slack.com/web)