/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-slack',
      type: 'Slack',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Slack = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Slack Adapter Test', () => {
  describe('Slack Class Tests', () => {
    const a = new Slack(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-slack-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-slack-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */
    // As a part of test setup, make sure that all constants below are valid in your Jira environment
    // You should modify the user ID of the other user to a valid one
    const otherUserId = 'CHANGE_ME';

    const Consts = {
      fake: 'fakedata',
      channelName: 'ittest',
      newChannelName: 'ittest_new',
      channelPurpose: 'Integration Test Purpose',
      channelTopic: 'Integration Test Topic',
      otherUser: otherUserId,
      slackbot: 'USLACKBOT',
      chatText: 'This is a test message',
      chatTextNew: 'This is an updated message',
      conversationName: 'test_convo',
      conversationPurpose: 'To test conversations',
      conversationTopic: 'test conversation'
    };

    describe('System Integration Tests', () => {
      describe('#getApitest - errors', () => {
        it('should work if integrated or standalone with mockdata', (done) => {
          try {
            let foo;
            let fooerr;
            if (stub) {
              foo = Consts.fake;
              fooerr = Consts.fake;
            }
            a.getApitest(foo, fooerr, (data, error) => {
              try {
                if (stub) {
                  assert.equal(undefined, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(true, data.response.ok);
                } else {
                  assert.equal(undefined, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.notEqual(null, data.response);
                }

                done();
              } catch (err) {
                log.error(`Test Failure: ${err}`);
                done(err);
              }
            });
          } catch (error) {
            log.error(`Adapter Exception: ${error}`);
            done(error);
          }
        }).timeout(attemptTimeout);
      });

      describe('#getAuthtest - errors', () => {
        it('should work if integrated or standalone with mockdata', (done) => {
          try {
            a.getAuthtest(pronghornProps.adapterProps.adapters[0].properties.authentication.token, (data, error) => {
              try {
                if (stub) {
                  assert.equal(undefined, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(true, data.response.ok);
                  assert.equal('Subarachnoid Workspace', data.response.team);
                  assert.equal('T12345678', data.response.team_id);
                  assert.equal('https://subarachnoid.slack.com/', data.response.url);
                  assert.equal('grace', data.response.user);
                  assert.equal('W12345678', data.response.user_id);
                } else {
                  assert.equal(undefined, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.notEqual(null, data.response);
                  assert.notEqual(undefined, data.response.user);
                  assert.notEqual(undefined, data.response.user_id);
                }

                done();
              } catch (err) {
                log.error(`Test Failure: ${err}`);
                done(err);
              }
            });
          } catch (error) {
            log.error(`Adapter Exception: ${error}`);
            done(error);
          }
        }).timeout(attemptTimeout);
      });

      describe('Channel Entity', () => {
        let newChannelId;

        describe('#postChannelscreate - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let validate;
              let token;
              let name;
              if (stub) {
                validate = Consts.fake;
                token = Consts.fake;
                name = Consts.fake;
              } else {
                validate = true;
                name = Consts.channelName;
              }

              a.postChannelscreate(validate, token, name, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('object', typeof data.response.channel);
                    assert.equal(true, data.response.ok);
                  } else {
                    newChannelId = data.response.channel.id;
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                    assert.equal(name, data.response.channel.name);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getChannelslist - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let excludeMembers;
              let cursor;
              let userToken;
              let limit;
              let excludeArchived;
              if (stub) {
                excludeMembers = Consts.fake;
                cursor = Consts.fake;
                userToken = Consts.fake;
                limit = Consts.fake;
                excludeArchived = Consts.fake;
              }
              a.getChannelslist(excludeMembers, cursor, userToken, limit, excludeArchived, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal(true, Array.isArray(data.response.channels));
                    assert.equal(true, data.response.ok);
                    assert.equal('object', typeof data.response.response_metadata);
                  } else {
                    const channelIds = data.response.channels.map((c) => c.id);
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                    assert.notEqual(undefined, data.response.channels);
                    assert.notEqual(0, data.response.channels.length);
                    assert.equal(true, channelIds.includes(newChannelId));
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getChannelsinfo - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let token;
              let includeLocale;
              let channel;
              if (stub) {
                token = Consts.fake;
                includeLocale = Consts.fake;
                channel = Consts.fake;
              } else {
                channel = newChannelId;
              }
              a.getChannelsinfo(token, includeLocale, channel, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('object', typeof data.response.channel);
                    assert.equal(true, data.response.ok);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#postChannelsrename - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let validate;
              let token;
              let name;
              let channel;
              if (stub) {
                validate = Consts.fake;
                name = Consts.fake;
                token = Consts.fake;
                channel = Consts.fake;
              } else {
                channel = newChannelId;
                name = Consts.newChannelName;
              }
              a.postChannelsrename(validate, token, name, channel, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('object', typeof data.response.channel);
                    assert.equal(true, data.response.ok);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#postChannelssetPurpose - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let token;
              let channel;
              let purpose;
              if (stub) {
                token = Consts.fake;
                channel = Consts.fake;
                purpose = Consts.fake;
              } else {
                channel = newChannelId;
                purpose = Consts.channelPurpose;
              }
              a.postChannelssetPurpose(token, purpose, channel, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal(true, data.response.ok);
                    assert.equal('My special purpose', data.response.purpose);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#postChannelssetTopic - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let token;
              let channel;
              let topic;
              if (stub) {
                token = Consts.fake;
                channel = Consts.fake;
                topic = Consts.fake;
              } else {
                channel = newChannelId;
                topic = Consts.channelTopic;
              }
              a.postChannelssetTopic(topic, token, channel, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal(true, data.response.ok);
                    assert.equal('To picture topicality', data.response.topic);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#postChannelsjoin - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let token;
              let name;
              let validate;
              if (stub) {
                token = Consts.fake;
                name = Consts.fake;
                validate = Consts.fake;
              } else {
                name = Consts.newChannelName;
              }
              a.postChannelsjoin(validate, token, name, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal(true, data.response.already_in_channel);
                    assert.equal('object', typeof data.response.channel);
                    assert.equal(true, data.response.ok);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#postChannelsinvite - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let token;
              let user;
              let channel;
              if (stub) {
                token = Consts.fake;
                user = Consts.fake;
                channel = Consts.fake;
              } else {
                user = Consts.otherUser;
                channel = newChannelId;
              }
              a.postChannelsinvite(token, user, channel, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('object', typeof data.response.channel);
                    assert.equal(true, data.response.ok);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#postChannelskick - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let token;
              let user;
              let channel;
              if (stub) {
                token = Consts.fake;
                user = Consts.fake;
                channel = Consts.fake;
              } else {
                user = Consts.otherUser;
                channel = newChannelId;
              }
              a.postChannelskick(token, user, channel, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal(true, data.response.ok);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#postChannelsarchive - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let token;
              let channel;

              if (stub) {
                token = Consts.fake;
                channel = Consts.fake;
              } else {
                channel = newChannelId;
              }
              a.postChannelsarchive(token, channel, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal(true, data.response.ok);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });
      });

      describe('Chat Entity', () => {
        let channelId;
        let messageTs;
        mocha.before(() => {
          let getChannelPromise;
          if (!stub) {
            getChannelPromise = new Promise((resolve) => {
              let excludeMembers;
              let cursor;
              let userToken;
              let limit;
              let excludeArchived;
              a.getChannelslist(excludeMembers, cursor, userToken, limit, excludeArchived, (data) => {
                channelId = data.response.channels[0].id;
                resolve();
              });
            });
          }
          return getChannelPromise;
        });

        describe('#postChatpostMessage - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let user;
              let threadTs;
              let attachments;
              let unfurlLinks;
              let text;
              let unfurlMedia;
              let parse;
              let asUser;
              let token;
              let mrkdwn;
              let iconEmoji;
              let linkNames;
              let iconUrl;
              let channel;
              let replyBroadcast;
              if (stub) {
                user = Consts.fake;
                threadTs = Consts.fake;
                attachments = Consts.fake;
                unfurlLinks = Consts.fake;
                text = Consts.fake;
                unfurlMedia = Consts.fake;
                parse = Consts.fake;
                asUser = Consts.fake;
                token = Consts.fake;
                mrkdwn = Consts.fake;
                iconEmoji = Consts.fake;
                linkNames = Consts.fake;
                iconUrl = Consts.fake;
                channel = Consts.fake;
                replyBroadcast = Consts.fake;
              } else {
                channel = channelId;
                text = Consts.chatText;
              }
              a.postChatpostMessage(user, threadTs, attachments, unfurlLinks, text, unfurlMedia, parse, asUser, token, mrkdwn, iconEmoji, linkNames, iconUrl, channel, replyBroadcast, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('C1H9RESGL', data.response.channel);
                    assert.equal('object', typeof data.response.message);
                    assert.equal(true, data.response.ok);
                    assert.equal('1503435956.000247', data.response.ts);
                  } else {
                    messageTs = data.response.ts;
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#postChatupdate - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let attachments;
              let text;
              let ts;
              let parse;
              let asUser;
              let token;
              let linkNames;
              let channel;
              if (stub) {
                attachments = Consts.fake;
                text = Consts.fake;
                ts = Consts.fake;
                parse = Consts.fake;
                parse = Consts.fake;
                asUser = Consts.fake;
                token = Consts.fake;
                token = Consts.fake;
                linkNames = Consts.fake;
                channel = Consts.fake;
              } else {
                ts = messageTs;
                channel = channelId;
                text = Consts.chatTextNew;
              }
              a.postChatupdate(attachments, text, ts, parse, asUser, token, linkNames, channel, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('C024BE91L', data.response.channel);
                    assert.equal(true, data.response.ok);
                    assert.equal('Updated text you carefully authored', data.response.text);
                    assert.equal('1401383885.000061', data.response.ts);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#postChatdelete - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let asUser;
              let token;
              let ts;
              let channel;
              if (stub) {
                asUser = Consts.fake;
                token = Consts.fake;
                ts = Consts.fake;
                channel = Consts.fake;
              } else {
                ts = messageTs;
                channel = channelId;
              }
              a.postChatdelete(asUser, token, ts, channel, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('C024BE91L', data.response.channel);
                    assert.equal(true, data.response.ok);
                    assert.equal('1401383885.000061', data.response.ts);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });
      });

      describe('User Entity', () => {
        describe('#getUsersinfo - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let includeLocale;
              let token;
              let user;
              if (stub) {
                includeLocale = Consts.fake;
                token = Consts.fake;
                user = Consts.fake;
              } else {
                user = Consts.otherUser;
              }
              a.getUsersinfo(includeLocale, token, user, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal(true, data.response.ok);
                    assert.equal('object', typeof data.response.user);
                  } else {
                    assert.equal(data.response.user.id, Consts.otherUser);
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getUserslist - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let presence;
              let cursor;
              let token;
              let limit;
              let includeLocale;
              if (stub) {
                presence = Consts.fake;
                cursor = Consts.fake;
                token = Consts.fake;
                limit = Consts.fake;
                includeLocale = Consts.fake;
              } else {
                limit = 1;
              }
              a.getUserslist(presence, cursor, token, limit, includeLocale, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal(1498777272, data.response.cache_ts);
                    assert.equal(true, Array.isArray(data.response.members));
                    assert.equal(true, data.response.ok);
                    assert.equal('object', typeof data.response.response_metadata);
                  } else {
                    assert.equal(1, data.response.members.length);
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });
      });

      describe('IM Entity', () => {
        let imId;
        describe('#postImopen - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let token;
              let returnIm;
              let user;
              let includeLocale;
              if (stub) {
                token = Consts.fake;
                returnIm = Consts.fake;
                user = Consts.fake;
                includeLocale = Consts.fake;
              } else {
                user = Consts.otherUser;
                returnIm = true;
              }
              a.postImopen(token, returnIm, user, includeLocale, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('object', typeof data.response.channel);
                    assert.equal(true, data.response.ok);
                  } else {
                    imId = data.response.channel.id;
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getImlist - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let cursor;
              let token;
              let limit;
              if (stub) {
                cursor = Consts.fake;
                token = Consts.fake;
                limit = Consts.fake;
              }
              a.getImlist(cursor, token, limit, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal(true, Array.isArray(data.response.ims));
                    assert.equal(true, data.response.ok);
                    assert.equal('object', typeof data.response.response_metadata);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#postImclose - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let token;
              let channel;
              if (stub) {
                token = Consts.fake;
                channel = Consts.fake;
              } else {
                channel = imId;
              }
              a.postImclose(token, channel, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal(true, data.response.ok);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });
      });

      describe('Conversation Entity', () => {
        let convoId;
        describe('#postConversationscreate - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let token;
              let name;
              let isPrivate;
              if (stub) {
                token = Consts.fake;
                name = Consts.fake;
                isPrivate = Consts.fake;
              } else {
                name = Consts.conversationName;
                isPrivate = true;
              }
              a.postConversationscreate(token, name, isPrivate, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('object', typeof data.response.channel);
                    assert.equal(true, data.response.ok);
                  } else {
                    convoId = data.response.channel.id;
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                    assert.equal('object', typeof data.response.channel);
                    assert.equal(true, data.response.ok);
                    assert.equal(Consts.conversationName, data.response.channel.name);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getConversationsinfo - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let token;
              let channel;
              let includeLocale;
              if (stub) {
                token = Consts.fake;
                channel = Consts.fake;
                includeLocale = Consts.fake;
              } else {
                channel = convoId;
              }
              a.getConversationsinfo(token, channel, includeLocale, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('object', typeof data.response.channel);
                    assert.equal(true, data.response.ok);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                    assert.equal('object', typeof data.response.channel);
                    assert.equal(true, data.response.ok);
                    assert.equal(convoId, data.response.channel.id);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#postConversationsinvite - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let token;
              let users;
              let channel;
              if (stub) {
                token = Consts.fake;
                users = Consts.fake;
                channel = Consts.fake;
              } else {
                users = [Consts.otherUser];
                channel = convoId;
              }
              a.postConversationsinvite(token, users, channel, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal('object', typeof data.response.channel);
                    assert.equal(true, data.response.ok);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                    assert.equal('object', typeof data.response.channel);
                    assert.equal(true, data.response.ok);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getConversationsmembers - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let cursor;
              let token;
              let limit;
              let channel;
              if (stub) {
                cursor = Consts.fake;
                token = Consts.fake;
                limit = Consts.fake;
                channel = Consts.fake;
              } else {
                channel = convoId;
              }
              a.getConversationsmembers(cursor, token, limit, channel, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal(true, Array.isArray(data.response.members));
                    assert.equal(true, data.response.ok);
                    assert.equal('object', typeof data.response.response_metadata);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                    assert.equal(true, data.response.members.length >= 2);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#postConversationskick - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let token;
              let user;
              let channel;
              if (stub) {
                token = Consts.fake;
                user = Consts.fake;
                channel = Consts.fake;
              } else {
                user = Consts.otherUser;
                channel = convoId;
              }
              a.postConversationskick(token, user, channel, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal(true, data.response.ok);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                    assert.equal(true, data.response.ok);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#getConversationslist - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let cursor;
              let token;
              let limit;
              let excludeArchived;
              let types;
              if (stub) {
                cursor = Consts.fake;
                token = Consts.fake;
                limit = Consts.fake;
                excludeArchived = Consts.fake;
                types = Consts.fake;
              }
              a.getConversationslist(cursor, token, limit, excludeArchived, types, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal(true, Array.isArray(data.response.channels));
                    assert.equal(true, data.response.ok);
                    assert.equal('object', typeof data.response.response_metadata);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                    assert.equal(true, data.response.channels.length >= 1);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#postConversationssetPurpose - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let token;
              let purpose;
              let channel;
              if (stub) {
                token = Consts.fake;
                purpose = Consts.fake;
                channel = Consts.fake;
              } else {
                channel = convoId;
                purpose = Consts.conversationPurpose;
              }
              a.postConversationssetPurpose(token, purpose, channel, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal(true, data.response.ok);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                    assert.equal('object', typeof data.response.channel.purpose);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#postConversationssetTopic - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let topic;
              let token;
              let channel;
              if (stub) {
                topic = Consts.fake;
                token = Consts.fake;
                channel = Consts.fake;
              } else {
                channel = convoId;
                topic = Consts.conversationTopic;
              }
              a.postConversationssetTopic(topic, token, channel, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal(true, data.response.ok);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                    assert.equal('object', typeof data.response.channel.topic);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });

        describe('#postConversationsarchive - errors', () => {
          it('should work if integrated or standalone with mockdata', (done) => {
            try {
              let token;
              let channel;
              if (stub) {
                token = Consts.fake;
                channel = Consts.fake;
              } else {
                channel = convoId;
              }
              a.postConversationsarchive(token, channel, (data, error) => {
                try {
                  if (stub) {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.equal(true, data.response.ok);
                  } else {
                    assert.equal(undefined, error);
                    assert.notEqual(undefined, data);
                    assert.notEqual(null, data);
                    assert.notEqual(null, data.response);
                    assert.equal(true, data.response.ok);
                  }

                  done();
                } catch (err) {
                  log.error(`Test Failure: ${err}`);
                  done(err);
                }
              });
            } catch (error) {
              log.error(`Adapter Exception: ${error}`);
              done(error);
            }
          }).timeout(attemptTimeout);
        });
      });
    });

    describe('#getAppspermissionsinfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAppspermissionsinfo('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('object', typeof data.response.info);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppspermissionsrequest - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAppspermissionsrequest('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppspermissionsresourceslist - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAppspermissionsresourceslist('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
                assert.equal(true, Array.isArray(data.response.resources));
                assert.equal('object', typeof data.response.response_metadata);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppspermissionsscopeslist - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAppspermissionsscopeslist('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
                assert.equal('object', typeof data.response.scopes);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthrevoke - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAuthrevoke('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
                assert.equal(true, data.response.revoked);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBotsinfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBotsinfo('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('object', typeof data.response.bot);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getChannelshistory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getChannelshistory('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(false, data.response.has_more);
                assert.equal('1358547726.000003', data.response.latest);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postChannelsleave - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postChannelsleave('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postChannelsmark - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postChannelsmark('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getchannelsreplies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getChannelsreplies('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(false, data.response.has_more);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postChannelsunarchive - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postChannelsunarchive('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getChatgetPermalink - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getChatgetPermalink('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('C1H9RESGA', data.response.channel);
                assert.equal(true, data.response.ok);
                assert.equal('https://ghostbusters.slack.com/archives/C1H9RESGA/p135854651500008', data.response.permalink);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postChatmeMessage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postChatmeMessage('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('C024BE7LR', data.response.channel);
                assert.equal(true, data.response.ok);
                assert.equal('1417671948.000006', data.response.ts);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postChatpostEphemeral - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postChatpostEphemeral('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('1502210682.580145', data.response.message_ts);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postChatunfurl - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postChatunfurl('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConversationshistory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConversationshistory('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.has_more);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, data.response.ok);
                assert.equal(0, data.response.pin_count);
                assert.equal('object', typeof data.response.response_metadata);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postConversationsclose - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postConversationsclose('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postConversationsjoin - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postConversationsjoin('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('object', typeof data.response.channel);
                assert.equal(true, data.response.ok);
                assert.equal('object', typeof data.response.response_metadata);
                assert.equal('already_in_channel', data.response.warning);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postConversationsleave - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postConversationsleave('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postConversationsopen - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postConversationsopen('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('object', typeof data.response.channel);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postConversationsrename - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postConversationsrename('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('object', typeof data.response.channel);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConversationsreplies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConversationsreplies('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.has_more);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, data.response.ok);
                assert.equal('object', typeof data.response.response_metadata);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postConversationsunarchive - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postConversationsunarchive('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDialogopen - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDialogopen('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDndendDnd - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDndendDnd('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDndendSnooze - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDndendSnooze('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDndinfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDndinfo('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDndsetSnooze - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDndsetSnooze('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDndteamInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDndteamInfo('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
                assert.equal('object', typeof data.response.users);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEmojilist - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getEmojilist('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postFilescommentsadd - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postFilescommentsadd('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('object', typeof data.response.comment);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postFilescommentsdelete - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postFilescommentsdelete('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postFilescommentsedit - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postFilescommentsedit('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
                assert.equal('Subarachnoid Workspace', data.response.team);
                assert.equal('T12345678', data.response.team_id);
                assert.equal('https://subarachnoid.slack.com/', data.response.url);
                assert.equal('grace', data.response.user);
                assert.equal('W12345678', data.response.user_id);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postFilesdelete - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postFilesdelete('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFilesinfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFilesinfo('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFileslist - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFileslist('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postFilesrevokePublicURL - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postFilesrevokePublicURL('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postFilessharedPublicURL - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postFilessharedPublicURL('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postFilesupload - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postFilesupload('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 0, (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGroupsarchive - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postGroupsarchive('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGroupscreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postGroupscreate('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGroupscreateChild - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postGroupscreateChild('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroupshistory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGroupshistory('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(false, data.response.has_more);
                assert.equal('1358547726.000003', data.response.latest);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroupsinfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGroupsinfo('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGroupsinvite - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postGroupsinvite('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGroupskick - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postGroupskick('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGroupsleave - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postGroupsleave('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroupslist - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGroupslist('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGroupsmark - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postGroupsmark('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGroupsopen - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postGroupsopen('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGroupsrename - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postGroupsrename('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroupsreplies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGroupsreplies('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGroupssetPurpose - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postGroupssetPurpose('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGroupssetTopic - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postGroupssetTopic('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGroupsunarchive - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postGroupsunarchive('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImhistory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getImhistory('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(false, data.response.has_more);
                assert.equal('1358547726.000003', data.response.latest);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postImmark - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postImmark('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImreplies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getImreplies('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMigrationexchange - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMigrationexchange('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('E1KQTNXE1', data.response.enterprise_id);
                assert.equal(true, Array.isArray(data.response.invalid_user_ids));
                assert.equal(true, data.response.ok);
                assert.equal('T1KR7PE1W', data.response.team_id);
                assert.equal('object', typeof data.response.user_id_map);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postMpimclose - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postMpimclose('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMpimhistory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMpimhistory('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(false, data.response.has_more);
                assert.equal('1358547726.000003', data.response.latest);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMpimlist - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMpimlist('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postMpimmark - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postMpimmark('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postMpimopen - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postMpimopen('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('object', typeof data.response.channel);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMpimreplies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMpimreplies('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, Array.isArray(data.response.messages));
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOauthaccess - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOauthaccess('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('xoxp-XXXXXXXX-XXXXXXXX-XXXXX', data.response.access_token);
                assert.equal('groups:write', data.response.scope);
                assert.equal('TXXXXXXXXX', data.response.team_id);
                assert.equal('Wyld Stallyns LLC', data.response.team_name);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOauthtoken - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOauthtoken('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('xoxa-access-token-string', data.response.access_token);
                assert.equal('A012345678', data.response.app_id);
                assert.equal('U0AB12ABC', data.response.app_user_id);
                assert.equal('U0HTT3Q0G', data.response.authorizing_user_id);
                assert.equal('U061F7AUR', data.response.installer_user_id);
                assert.equal(true, data.response.ok);
                assert.equal(true, Array.isArray(data.response.permissions));
                assert.equal('C061EG9T2', data.response.single_channel_id);
                assert.equal('T061EG9Z9', data.response.team_id);
                assert.equal('Subarachnoid Workspace', data.response.team_name);
                assert.equal('app', data.response.token_type);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPinsadd - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postPinsadd('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPinslist - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPinslist('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPinsremove - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postPinsremove('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postReactionsadd - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postReactionsadd('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReactionsget - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getReactionsget('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('object', typeof data.response.file);
                assert.equal(true, data.response.ok);
                assert.equal('file', data.response.type);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReactionslist - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getReactionslist('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal(true, data.response.ok);
                assert.equal('object', typeof data.response.paging);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postReactionsremove - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postReactionsremove('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRemindersadd - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRemindersadd('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postReminderscomplete - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postReminderscomplete('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRemindersdelete - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRemindersdelete('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRemindersinfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRemindersinfo('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReminderslist - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getReminderslist('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRtmconnect - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRtmconnect('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
                assert.equal('object', typeof data.response.self);
                assert.equal('object', typeof data.response.team);
                assert.equal('wss://...', data.response.url);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRtmstart - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRtmstart('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSearchall - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSearchall('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('object', typeof data.response.files);
                assert.equal('object', typeof data.response.messages);
                assert.equal(true, data.response.ok);
                assert.equal('object', typeof data.response.posts);
                assert.equal('diagram', data.response.query);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSearchfiles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSearchfiles('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('object', typeof data.response.files);
                assert.equal(true, data.response.ok);
                assert.equal('computer.gif', data.response.query);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSearchmessages - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSearchmessages('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal('object', typeof data.response.messages);
                assert.equal(true, data.response.ok);
                assert.equal('The meaning of life the universe and everything', data.response.query);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postStarsadd - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postStarsadd('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStarslist - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStarslist('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postStarsremove - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postStarsremove('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeamaccessLogs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTeamaccessLogs('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeambillableInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTeambillableInfo('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeaminfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTeaminfo('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
                assert.equal('object', typeof data.response.team);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeamintegrationLogs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTeamintegrationLogs('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTeamprofileget - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTeamprofileget('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
                assert.equal('object', typeof data.response.profile);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUsergroupscreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postUsergroupscreate('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUsergroupsdisable - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postUsergroupsdisable('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUsergroupsenable - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postUsergroupsenable('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsergroupslist - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsergroupslist('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
                assert.equal(true, Array.isArray(data.response.usergroups));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUsergroupsupdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postUsergroupsupdate('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
                assert.equal('object', typeof data.response.usergroup);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsergroupsuserslist - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsergroupsuserslist('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
                assert.equal(true, Array.isArray(data.response.users));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUsergroupsusersupdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postUsergroupsusersupdate('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
                assert.equal('object', typeof data.response.usergroup);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersconversations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsersconversations('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, Array.isArray(data.response.channels));
                assert.equal(true, data.response.ok);
                assert.equal('object', typeof data.response.response_metadata);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUsersdeletePhoto - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postUsersdeletePhoto('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersgetPresence - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsersgetPresence('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
                assert.equal('active', data.response.presence);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersidentity - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsersidentity('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
                assert.equal('object', typeof data.response.team);
                assert.equal('object', typeof data.response.user);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserslookupByEmail - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUserslookupByEmail('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
                assert.equal('object', typeof data.response.user);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUserssetActive - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postUserssetActive('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUserssetPhoto - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postUserssetPhoto('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUserssetPresence - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postUserssetPresence('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersprofileget - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsersprofileget('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
                assert.equal('object', typeof data.response.profile);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUsersprofileset - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postUsersprofileset('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(true, data.response.ok);
                assert.equal('object', typeof data.response.profile);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
