
## 3.2.4 [10-15-2024]

* Changes made at 2024.10.14_21:37PM

See merge request itentialopensource/adapters/adapter-slack!25

---

## 3.2.3 [09-17-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-slack!23

---

## 3.2.2 [08-15-2024]

* Changes made at 2024.08.14_19:58PM

See merge request itentialopensource/adapters/adapter-slack!22

---

## 3.2.1 [08-07-2024]

* Changes made at 2024.08.06_22:06PM

See merge request itentialopensource/adapters/adapter-slack!21

---

## 3.2.0 [07-19-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/notification-messaging/adapter-slack!20

---

## 3.1.4 [03-29-2024]

* Changes made at 2024.03.29_10:22AM

See merge request itentialopensource/adapters/notification-messaging/adapter-slack!19

---

## 3.1.3 [03-11-2024]

* Changes made at 2024.03.11_11:14AM

See merge request itentialopensource/adapters/notification-messaging/adapter-slack!18

---

## 3.1.2 [02-28-2024]

* Changes made at 2024.02.28_12:33PM

See merge request itentialopensource/adapters/notification-messaging/adapter-slack!17

---

## 3.1.1 [01-01-2024]

* Update metadata

See merge request itentialopensource/adapters/notification-messaging/adapter-slack!16

---

## 3.1.0 [01-01-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/notification-messaging/adapter-slack!15

---

## 3.0.0 [02-15-2023]

* Major/adapt 2297

See merge request itentialopensource/adapters/notification-messaging/adapter-slack!14

---

## 2.6.2 [08-03-2022]

* Add parameters for postFilesupload

See merge request itentialopensource/adapters/notification-messaging/adapter-slack!13

---

## 2.6.1 [07-25-2022]

* Fix schema for threadTs

See merge request itentialopensource/adapters/notification-messaging/adapter-slack!11

---

## 2.6.0 [05-26-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/notification-messaging/adapter-slack!10

---

## 2.5.5 [03-15-2021] & 2.5.3 [03-15-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/notification-messaging/adapter-slack!8

---

## 2.5.2 [07-10-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/notification-messaging/adapter-slack!7

---

## 2.5.1 [01-14-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/notification-messaging/adapter-slack!6

---

## 2.5.0 [11-08-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/notification-messaging/adapter-slack!5

---

## 2.4.0 [09-16-2019]

- Update the adapter to the latest adapter foundation

See merge request itentialopensource/adapters/notification-messaging/adapter-slack!4

---
## 2.3.3 [07-31-2019] & 2.3.2 [07-30-2019] & 2.3.1 [07-18-2019]

- Bug fixes and performance improvements

See commit 4f0b35b

---

## 2.3.0 [07-18-2019] & 2.2.0 [07-18-2019]

- Migrate to the latest adapter foundation, categorization and prepare for app artifact

See merge request itentialopensource/adapters/notification-messaging/adapter-slack!3

---

## 2.1.0 [04-26-2019]

- Add workflow examples for Slack adapter and update readme file.

See merge request itentialopensource/adapters/adapter-slack!2

---

## 2.0.0 [04-12-2019] & 1.0.0 [04-12-2019]

- Initial Release of Slack adapter for Itential Automation Platform.

See merge request itentialopensource/adapters/adapter-slack!1

---

##  [04-11-2019] & [04-11-2019] & [04-11-2019]

- Initial Commit

See commit e1046d2

---
