## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Slack. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Slack.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Adapter for Slack. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getApitest(foo, error, callback)</td>
    <td style="padding:15px">Checks API calling code.</td>
    <td style="padding:15px">{base_path}/{version}/api.test?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppspermissionsinfo(token, callback)</td>
    <td style="padding:15px">Returns list of permissions this app has on a team.</td>
    <td style="padding:15px">{base_path}/{version}/apps.permissions.info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppspermissionsrequest(scopes, token, triggerId, callback)</td>
    <td style="padding:15px">Allows an app to request additional scopes</td>
    <td style="padding:15px">{base_path}/{version}/apps.permissions.request?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppspermissionsresourceslist(cursor, token, limit, callback)</td>
    <td style="padding:15px">Returns list of resource grants this app has on a team.</td>
    <td style="padding:15px">{base_path}/{version}/apps.permissions.resources.list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppspermissionsscopeslist(token, callback)</td>
    <td style="padding:15px">Returns list of scopes this app has on a team.</td>
    <td style="padding:15px">{base_path}/{version}/apps.permissions.scopes.list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthrevoke(test, token, callback)</td>
    <td style="padding:15px">Revokes a token.</td>
    <td style="padding:15px">{base_path}/{version}/auth.revoke?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthtest(token, callback)</td>
    <td style="padding:15px">Checks authentication & identity.</td>
    <td style="padding:15px">{base_path}/{version}/auth.test?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBotsinfo(token, bot, callback)</td>
    <td style="padding:15px">Gets information about a bot user.</td>
    <td style="padding:15px">{base_path}/{version}/bots.info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postChannelsarchive(token, channel, callback)</td>
    <td style="padding:15px">Archives a channel.</td>
    <td style="padding:15px">{base_path}/{version}/channels.archive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postChannelscreate(validate, token, name, callback)</td>
    <td style="padding:15px">Creates a channel.</td>
    <td style="padding:15px">{base_path}/{version}/channels.create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getChannelshistory(count, unreads, inclusive, token, oldest, channel, latest, callback)</td>
    <td style="padding:15px">Fetches history of messages and events from a channel.</td>
    <td style="padding:15px">{base_path}/{version}/channels.history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getChannelsinfo(token, includeLocale, channel, callback)</td>
    <td style="padding:15px">Gets information about a channel.</td>
    <td style="padding:15px">{base_path}/{version}/channels.info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postChannelsinvite(token, user, channel, callback)</td>
    <td style="padding:15px">Invites a user to a channel.</td>
    <td style="padding:15px">{base_path}/{version}/channels.invite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postChannelsjoin(validate, token, name, callback)</td>
    <td style="padding:15px">Joins a channel, creating it if needed.</td>
    <td style="padding:15px">{base_path}/{version}/channels.join?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postChannelskick(token, user, channel, callback)</td>
    <td style="padding:15px">Removes a user from a channel.</td>
    <td style="padding:15px">{base_path}/{version}/channels.kick?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postChannelsleave(token, channel, callback)</td>
    <td style="padding:15px">Leaves a channel.</td>
    <td style="padding:15px">{base_path}/{version}/channels.leave?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getChannelslist(excludeMembers, cursor, token, limit, excludeArchived, callback)</td>
    <td style="padding:15px">Lists all channels in a Slack team.</td>
    <td style="padding:15px">{base_path}/{version}/channels.list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postChannelsmark(token, ts, channel, callback)</td>
    <td style="padding:15px">Sets the read cursor in a channel.</td>
    <td style="padding:15px">{base_path}/{version}/channels.mark?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postChannelsrename(validate, token, name, channel, callback)</td>
    <td style="padding:15px">Renames a channel.</td>
    <td style="padding:15px">{base_path}/{version}/channels.rename?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getChannelsreplies(threadTs, token, channel, callback)</td>
    <td style="padding:15px">Retrieve a thread of messages posted to a channel</td>
    <td style="padding:15px">{base_path}/{version}/channels.replies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postChannelssetPurpose(token, purpose, channel, callback)</td>
    <td style="padding:15px">Sets the purpose for a channel.</td>
    <td style="padding:15px">{base_path}/{version}/channels.setPurpose?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postChannelssetTopic(topic, token, channel, callback)</td>
    <td style="padding:15px">Sets the topic for a channel.</td>
    <td style="padding:15px">{base_path}/{version}/channels.setTopic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postChannelsunarchive(token, channel, callback)</td>
    <td style="padding:15px">Unarchives a channel.</td>
    <td style="padding:15px">{base_path}/{version}/channels.unarchive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postChatdelete(asUser, token, ts, channel, callback)</td>
    <td style="padding:15px">Deletes a message.</td>
    <td style="padding:15px">{base_path}/{version}/chat.delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getChatgetPermalink(token, messageTs, channel, callback)</td>
    <td style="padding:15px">Retrieve a permalink URL for a specific extant message</td>
    <td style="padding:15px">{base_path}/{version}/chat.getPermalink?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postChatmeMessage(text, token, channel, callback)</td>
    <td style="padding:15px">Share a me message into a channel.</td>
    <td style="padding:15px">{base_path}/{version}/chat.meMessage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postChatpostEphemeral(attachments, text, linkNames, parse, token, user, asUser, channel, callback)</td>
    <td style="padding:15px">Sends an ephemeral message to a user in a channel.</td>
    <td style="padding:15px">{base_path}/{version}/chat.postEphemeral?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postChatpostMessage(username, threadTs, attachments, unfurlLinks, text, unfurlMedia, parse, asUser, token, mrkdwn, iconEmoji, linkNames, iconUrl, channel, replyBroadcast, callback)</td>
    <td style="padding:15px">Sends a message to a channel.</td>
    <td style="padding:15px">{base_path}/{version}/chat.postMessage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postChatunfurl(userAuthMessage, userAuthRequired, unfurls, ts, userAuthUrl, token, channel, callback)</td>
    <td style="padding:15px">Provide custom unfurl behavior for user-posted URLs</td>
    <td style="padding:15px">{base_path}/{version}/chat.unfurl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postChatupdate(attachments, text, ts, parse, asUser, token, linkNames, channel, callback)</td>
    <td style="padding:15px">Updates a message.</td>
    <td style="padding:15px">{base_path}/{version}/chat.update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConversationsarchive(token, channel, callback)</td>
    <td style="padding:15px">Archives a conversation.</td>
    <td style="padding:15px">{base_path}/{version}/conversations.archive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConversationsclose(token, channel, callback)</td>
    <td style="padding:15px">Closes a direct message or multi-person direct message.</td>
    <td style="padding:15px">{base_path}/{version}/conversations.close?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConversationscreate(token, name, isPrivate, callback)</td>
    <td style="padding:15px">Initiates a public or private channel-based conversation</td>
    <td style="padding:15px">{base_path}/{version}/conversations.create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConversationshistory(inclusive, cursor, token, limit, oldest, channel, latest, callback)</td>
    <td style="padding:15px">Fetches a conversation's history of messages and events.</td>
    <td style="padding:15px">{base_path}/{version}/conversations.history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConversationsinfo(token, channel, includeLocale, callback)</td>
    <td style="padding:15px">Retrieve information about a conversation.</td>
    <td style="padding:15px">{base_path}/{version}/conversations.info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConversationsinvite(token, users, channel, callback)</td>
    <td style="padding:15px">Invites users to a channel.</td>
    <td style="padding:15px">{base_path}/{version}/conversations.invite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConversationsjoin(token, channel, callback)</td>
    <td style="padding:15px">Joins an existing conversation.</td>
    <td style="padding:15px">{base_path}/{version}/conversations.join?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConversationskick(token, user, channel, callback)</td>
    <td style="padding:15px">Removes a user from a conversation.</td>
    <td style="padding:15px">{base_path}/{version}/conversations.kick?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConversationsleave(token, channel, callback)</td>
    <td style="padding:15px">Leaves a conversation.</td>
    <td style="padding:15px">{base_path}/{version}/conversations.leave?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConversationslist(cursor, token, limit, excludeArchived, types, callback)</td>
    <td style="padding:15px">Lists all channels in a Slack team.</td>
    <td style="padding:15px">{base_path}/{version}/conversations.list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConversationsmembers(cursor, token, limit, channel, callback)</td>
    <td style="padding:15px">Retrieve members of a conversation.</td>
    <td style="padding:15px">{base_path}/{version}/conversations.members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConversationsopen(token, returnIm, users, channel, callback)</td>
    <td style="padding:15px">Opens or resumes a direct message or multi-person direct message.</td>
    <td style="padding:15px">{base_path}/{version}/conversations.open?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConversationsrename(token, name, channel, callback)</td>
    <td style="padding:15px">Renames a conversation.</td>
    <td style="padding:15px">{base_path}/{version}/conversations.rename?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConversationsreplies(inclusive, ts, cursor, token, limit, oldest, channel, latest, callback)</td>
    <td style="padding:15px">Retrieve a thread of messages posted to a conversation</td>
    <td style="padding:15px">{base_path}/{version}/conversations.replies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConversationssetPurpose(token, purpose, channel, callback)</td>
    <td style="padding:15px">Sets the purpose for a conversation.</td>
    <td style="padding:15px">{base_path}/{version}/conversations.setPurpose?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConversationssetTopic(topic, token, channel, callback)</td>
    <td style="padding:15px">Sets the topic for a conversation.</td>
    <td style="padding:15px">{base_path}/{version}/conversations.setTopic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postConversationsunarchive(token, channel, callback)</td>
    <td style="padding:15px">Reverses conversation archival.</td>
    <td style="padding:15px">{base_path}/{version}/conversations.unarchive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDialogopen(token, dialog, triggerId, callback)</td>
    <td style="padding:15px">Open a dialog with a user</td>
    <td style="padding:15px">{base_path}/{version}/dialog.open?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDndendDnd(token, callback)</td>
    <td style="padding:15px">Ends the current user's Do Not Disturb session immediately.</td>
    <td style="padding:15px">{base_path}/{version}/dnd.endDnd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDndendSnooze(token, callback)</td>
    <td style="padding:15px">Ends the current user's snooze mode immediately.</td>
    <td style="padding:15px">{base_path}/{version}/dnd.endSnooze?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDndinfo(token, user, callback)</td>
    <td style="padding:15px">Retrieves a user's current Do Not Disturb status.</td>
    <td style="padding:15px">{base_path}/{version}/dnd.info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDndsetSnooze(numMinutes, token, callback)</td>
    <td style="padding:15px">Turns on Do Not Disturb mode for the current user, or changes its duration.</td>
    <td style="padding:15px">{base_path}/{version}/dnd.setSnooze?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDndteamInfo(token, users, callback)</td>
    <td style="padding:15px">Retrieves the Do Not Disturb status for users on a team.</td>
    <td style="padding:15px">{base_path}/{version}/dnd.teamInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEmojilist(token, callback)</td>
    <td style="padding:15px">Lists custom emoji for a team.</td>
    <td style="padding:15px">{base_path}/{version}/emoji.list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFilescommentsadd(comment, token, file, callback)</td>
    <td style="padding:15px">Add a comment to an existing file.</td>
    <td style="padding:15px">{base_path}/{version}/files.comments.add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFilescommentsdelete(token, id, file, callback)</td>
    <td style="padding:15px">Deletes an existing comment on a file.</td>
    <td style="padding:15px">{base_path}/{version}/files.comments.delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFilescommentsedit(comment, token, id, file, callback)</td>
    <td style="padding:15px">Edit an existing file comment.</td>
    <td style="padding:15px">{base_path}/{version}/files.comments.edit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFilesdelete(token, file, callback)</td>
    <td style="padding:15px">Deletes a file.</td>
    <td style="padding:15px">{base_path}/{version}/files.delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFilesinfo(count, token, file, page, callback)</td>
    <td style="padding:15px">Gets information about a team file.</td>
    <td style="padding:15px">{base_path}/{version}/files.info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFileslist(count, channel, tsTo, tsFrom, token, user, page, types, callback)</td>
    <td style="padding:15px">Lists & filters team files.</td>
    <td style="padding:15px">{base_path}/{version}/files.list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFilesrevokePublicURL(token, file, callback)</td>
    <td style="padding:15px">Revokes public/external sharing access for a file</td>
    <td style="padding:15px">{base_path}/{version}/files.revokePublicURL?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFilessharedPublicURL(token, file, callback)</td>
    <td style="padding:15px">Enables a file for public/external sharing.</td>
    <td style="padding:15px">{base_path}/{version}/files.sharedPublicURL?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFilesupload(channels, title, initialComment, filetype, filename, content, token, file, threadTs, callback)</td>
    <td style="padding:15px">Uploads or creates a file.</td>
    <td style="padding:15px">{base_path}/{version}/files.upload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGroupsarchive(token, channel, callback)</td>
    <td style="padding:15px">Archives a private channel.</td>
    <td style="padding:15px">{base_path}/{version}/groups.archive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGroupscreate(validate, token, name, callback)</td>
    <td style="padding:15px">Creates a private channel.</td>
    <td style="padding:15px">{base_path}/{version}/groups.create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGroupscreateChild(token, channel, callback)</td>
    <td style="padding:15px">Clones and archives a private channel.</td>
    <td style="padding:15px">{base_path}/{version}/groups.createChild?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupshistory(count, unreads, inclusive, token, oldest, channel, latest, callback)</td>
    <td style="padding:15px">Fetches history of messages and events from a private channel.</td>
    <td style="padding:15px">{base_path}/{version}/groups.history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupsinfo(token, includeLocale, channel, callback)</td>
    <td style="padding:15px">Gets information about a private channel.</td>
    <td style="padding:15px">{base_path}/{version}/groups.info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGroupsinvite(token, user, channel, callback)</td>
    <td style="padding:15px">Invites a user to a private channel.</td>
    <td style="padding:15px">{base_path}/{version}/groups.invite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGroupskick(token, user, channel, callback)</td>
    <td style="padding:15px">Removes a user from a private channel.</td>
    <td style="padding:15px">{base_path}/{version}/groups.kick?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGroupsleave(token, channel, callback)</td>
    <td style="padding:15px">Leaves a private channel.</td>
    <td style="padding:15px">{base_path}/{version}/groups.leave?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupslist(excludeMembers, token, excludeArchived, callback)</td>
    <td style="padding:15px">Lists private channels that the calling user has access to.</td>
    <td style="padding:15px">{base_path}/{version}/groups.list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGroupsmark(token, ts, channel, callback)</td>
    <td style="padding:15px">Sets the read cursor in a private channel.</td>
    <td style="padding:15px">{base_path}/{version}/groups.mark?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGroupsopen(token, channel, callback)</td>
    <td style="padding:15px">Opens a private channel.</td>
    <td style="padding:15px">{base_path}/{version}/groups.open?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGroupsrename(validate, token, name, channel, callback)</td>
    <td style="padding:15px">Renames a private channel.</td>
    <td style="padding:15px">{base_path}/{version}/groups.rename?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupsreplies(threadTs, token, channel, callback)</td>
    <td style="padding:15px">Retrieve a thread of messages posted to a private channel</td>
    <td style="padding:15px">{base_path}/{version}/groups.replies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGroupssetPurpose(token, purpose, channel, callback)</td>
    <td style="padding:15px">Sets the purpose for a private channel.</td>
    <td style="padding:15px">{base_path}/{version}/groups.setPurpose?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGroupssetTopic(topic, token, channel, callback)</td>
    <td style="padding:15px">Sets the topic for a private channel.</td>
    <td style="padding:15px">{base_path}/{version}/groups.setTopic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGroupsunarchive(token, channel, callback)</td>
    <td style="padding:15px">Unarchives a private channel.</td>
    <td style="padding:15px">{base_path}/{version}/groups.unarchive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postImclose(token, channel, callback)</td>
    <td style="padding:15px">Close a direct message channel.</td>
    <td style="padding:15px">{base_path}/{version}/im.close?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImhistory(count, unreads, inclusive, token, oldest, channel, latest, callback)</td>
    <td style="padding:15px">Fetches history of messages and events from direct message channel.</td>
    <td style="padding:15px">{base_path}/{version}/im.history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImlist(cursor, token, limit, callback)</td>
    <td style="padding:15px">Lists direct message channels for the calling user.</td>
    <td style="padding:15px">{base_path}/{version}/im.list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postImmark(token, ts, channel, callback)</td>
    <td style="padding:15px">Sets the read cursor in a direct message channel.</td>
    <td style="padding:15px">{base_path}/{version}/im.mark?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postImopen(token, returnIm, user, includeLocale, callback)</td>
    <td style="padding:15px">Opens a direct message channel.</td>
    <td style="padding:15px">{base_path}/{version}/im.open?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImreplies(threadTs, token, channel, callback)</td>
    <td style="padding:15px">Retrieve a thread of messages posted to a direct message conversation</td>
    <td style="padding:15px">{base_path}/{version}/im.replies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMigrationexchange(token, toOld, users, callback)</td>
    <td style="padding:15px">For Enterprise Grid workspaces, map local user IDs to global user IDs</td>
    <td style="padding:15px">{base_path}/{version}/migration.exchange?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMpimclose(token, channel, callback)</td>
    <td style="padding:15px">Closes a multiparty direct message channel.</td>
    <td style="padding:15px">{base_path}/{version}/mpim.close?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMpimhistory(count, unreads, inclusive, token, oldest, channel, latest, callback)</td>
    <td style="padding:15px">Fetches history of messages and events from a multiparty direct message.</td>
    <td style="padding:15px">{base_path}/{version}/mpim.history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMpimlist(token, callback)</td>
    <td style="padding:15px">Lists multiparty direct message channels for the calling user.</td>
    <td style="padding:15px">{base_path}/{version}/mpim.list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMpimmark(token, ts, channel, callback)</td>
    <td style="padding:15px">Sets the read cursor in a multiparty direct message channel.</td>
    <td style="padding:15px">{base_path}/{version}/mpim.mark?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMpimopen(token, users, callback)</td>
    <td style="padding:15px">This method opens a multiparty direct message.</td>
    <td style="padding:15px">{base_path}/{version}/mpim.open?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMpimreplies(threadTs, token, channel, callback)</td>
    <td style="padding:15px">Retrieve a thread of messages posted to a direct message conversation from a multiparty direct message.</td>
    <td style="padding:15px">{base_path}/{version}/mpim.replies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOauthaccess(clientSecret, code, singleChannel, clientId, redirectUri, callback)</td>
    <td style="padding:15px">Exchanges a temporary OAuth verifier code for an access token.</td>
    <td style="padding:15px">{base_path}/{version}/oauth.access?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOauthtoken(clientSecret, code, singleChannel, clientId, redirectUri, callback)</td>
    <td style="padding:15px">Exchanges a temporary OAuth verifier code for a workspace token.</td>
    <td style="padding:15px">{base_path}/{version}/oauth.token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPinsadd(fileComment, timestamp, token, file, channel, callback)</td>
    <td style="padding:15px">Pins an item to a channel.</td>
    <td style="padding:15px">{base_path}/{version}/pins.add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPinslist(token, channel, callback)</td>
    <td style="padding:15px">Lists items pinned to a channel.</td>
    <td style="padding:15px">{base_path}/{version}/pins.list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPinsremove(fileComment, timestamp, token, file, channel, callback)</td>
    <td style="padding:15px">Un-pins an item from a channel.</td>
    <td style="padding:15px">{base_path}/{version}/pins.remove?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReactionsadd(name, fileComment, timestamp, token, file, channel, callback)</td>
    <td style="padding:15px">Adds a reaction to an item.</td>
    <td style="padding:15px">{base_path}/{version}/reactions.add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReactionsget(full, fileComment, timestamp, token, file, channel, callback)</td>
    <td style="padding:15px">Gets reactions for an item.</td>
    <td style="padding:15px">{base_path}/{version}/reactions.get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReactionslist(count, token, full, user, page, callback)</td>
    <td style="padding:15px">Lists reactions made by a user.</td>
    <td style="padding:15px">{base_path}/{version}/reactions.list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReactionsremove(name, fileComment, timestamp, token, file, channel, callback)</td>
    <td style="padding:15px">Removes a reaction from an item.</td>
    <td style="padding:15px">{base_path}/{version}/reactions.remove?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemindersadd(text, token, user, time, callback)</td>
    <td style="padding:15px">Creates a reminder.</td>
    <td style="padding:15px">{base_path}/{version}/reminders.add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReminderscomplete(token, reminder, callback)</td>
    <td style="padding:15px">Marks a reminder as complete.</td>
    <td style="padding:15px">{base_path}/{version}/reminders.complete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRemindersdelete(token, reminder, callback)</td>
    <td style="padding:15px">Deletes a reminder.</td>
    <td style="padding:15px">{base_path}/{version}/reminders.delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRemindersinfo(token, reminder, callback)</td>
    <td style="padding:15px">Gets information about a reminder.</td>
    <td style="padding:15px">{base_path}/{version}/reminders.info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReminderslist(token, callback)</td>
    <td style="padding:15px">Lists all reminders created by or for a given user.</td>
    <td style="padding:15px">{base_path}/{version}/reminders.list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRtmconnect(presenceSub, token, batchPresenceAware, callback)</td>
    <td style="padding:15px">Starts a Real Time Messaging session.</td>
    <td style="padding:15px">{base_path}/{version}/rtm.connect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRtmstart(noLatest, simpleLatest, includeLocale, presenceSub, noUnreads, batchPresenceAware, mpimAware, token, callback)</td>
    <td style="padding:15px">Starts a Real Time Messaging session.</td>
    <td style="padding:15px">{base_path}/{version}/rtm.start?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSearchall(sortDir, query, sort, count, token, highlight, page, callback)</td>
    <td style="padding:15px">Searches for messages and files matching a query.</td>
    <td style="padding:15px">{base_path}/{version}/search.all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSearchfiles(sortDir, query, sort, highlight, count, token, page, callback)</td>
    <td style="padding:15px">Searches for files matching a query.</td>
    <td style="padding:15px">{base_path}/{version}/search.files?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSearchmessages(sortDir, query, sort, count, token, highlight, page, callback)</td>
    <td style="padding:15px">Searches for messages matching a query.</td>
    <td style="padding:15px">{base_path}/{version}/search.messages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postStarsadd(fileComment, timestamp, token, channel, file, callback)</td>
    <td style="padding:15px">Adds a star to an item.</td>
    <td style="padding:15px">{base_path}/{version}/stars.add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStarslist(count, token, page, callback)</td>
    <td style="padding:15px">Lists stars for a user.</td>
    <td style="padding:15px">{base_path}/{version}/stars.list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postStarsremove(fileComment, timestamp, token, channel, file, callback)</td>
    <td style="padding:15px">Removes a star from an item.</td>
    <td style="padding:15px">{base_path}/{version}/stars.remove?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeamaccessLogs(count, token, page, before, callback)</td>
    <td style="padding:15px">Gets the access logs for the current team.</td>
    <td style="padding:15px">{base_path}/{version}/team.accessLogs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeambillableInfo(token, user, callback)</td>
    <td style="padding:15px">Gets billable users information for the current team.</td>
    <td style="padding:15px">{base_path}/{version}/team.billableInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeaminfo(token, callback)</td>
    <td style="padding:15px">Gets information about the current team.</td>
    <td style="padding:15px">{base_path}/{version}/team.info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeamintegrationLogs(count, changeType, appId, token, user, serviceId, page, callback)</td>
    <td style="padding:15px">Gets the integration logs for the current team.</td>
    <td style="padding:15px">{base_path}/{version}/team.integrationLogs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTeamprofileget(token, visibility, callback)</td>
    <td style="padding:15px">Retrieve a team's profile.</td>
    <td style="padding:15px">{base_path}/{version}/team.profile.get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUsergroupscreate(handle, name, channels, token, includeCount, description, callback)</td>
    <td style="padding:15px">Create a User Group</td>
    <td style="padding:15px">{base_path}/{version}/usergroups.create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUsergroupsdisable(token, includeCount, usergroup, callback)</td>
    <td style="padding:15px">Disable an existing User Group</td>
    <td style="padding:15px">{base_path}/{version}/usergroups.disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUsergroupsenable(token, includeCount, usergroup, callback)</td>
    <td style="padding:15px">Enable a User Group</td>
    <td style="padding:15px">{base_path}/{version}/usergroups.enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsergroupslist(includeUsers, token, includeCount, includeDisabled, callback)</td>
    <td style="padding:15px">List all User Groups for a team</td>
    <td style="padding:15px">{base_path}/{version}/usergroups.list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUsergroupsupdate(handle, description, channels, token, includeCount, usergroup, name, callback)</td>
    <td style="padding:15px">Update an existing User Group</td>
    <td style="padding:15px">{base_path}/{version}/usergroups.update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsergroupsuserslist(token, includeDisabled, usergroup, callback)</td>
    <td style="padding:15px">List all users in a User Group</td>
    <td style="padding:15px">{base_path}/{version}/usergroups.users.list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUsergroupsusersupdate(includeCount, token, users, usergroup, callback)</td>
    <td style="padding:15px">Update the list of users for a User Group</td>
    <td style="padding:15px">{base_path}/{version}/usergroups.users.update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersconversations(cursor, token, limit, user, excludeArchived, types, callback)</td>
    <td style="padding:15px">List conversations the calling user may access.</td>
    <td style="padding:15px">{base_path}/{version}/users.conversations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUsersdeletePhoto(token, callback)</td>
    <td style="padding:15px">Delete the user profile photo</td>
    <td style="padding:15px">{base_path}/{version}/users.deletePhoto?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersgetPresence(token, user, callback)</td>
    <td style="padding:15px">Gets user presence information.</td>
    <td style="padding:15px">{base_path}/{version}/users.getPresence?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersidentity(token, callback)</td>
    <td style="padding:15px">Get a user's identity.</td>
    <td style="padding:15px">{base_path}/{version}/users.identity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersinfo(includeLocale, token, user, callback)</td>
    <td style="padding:15px">Gets information about a user.</td>
    <td style="padding:15px">{base_path}/{version}/users.info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserslist(presence, cursor, token, limit, includeLocale, callback)</td>
    <td style="padding:15px">Lists all users in a Slack team.</td>
    <td style="padding:15px">{base_path}/{version}/users.list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserslookupByEmail(token, email, callback)</td>
    <td style="padding:15px">Find a user with an email address.</td>
    <td style="padding:15px">{base_path}/{version}/users.lookupByEmail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUserssetActive(token, callback)</td>
    <td style="padding:15px">Marked a user as active. Deprecated and non-functional.</td>
    <td style="padding:15px">{base_path}/{version}/users.setActive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUserssetPhoto(image, cropW, token, cropY, cropX, callback)</td>
    <td style="padding:15px">Set the user profile photo</td>
    <td style="padding:15px">{base_path}/{version}/users.setPhoto?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUserssetPresence(token, presence, callback)</td>
    <td style="padding:15px">Manually sets user presence.</td>
    <td style="padding:15px">{base_path}/{version}/users.setPresence?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersprofileget(token, includeLabels, user, callback)</td>
    <td style="padding:15px">Retrieves a user's profile information.</td>
    <td style="padding:15px">{base_path}/{version}/users.profile.get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUsersprofileset(profile, token, user, value, name, callback)</td>
    <td style="padding:15px">Set the profile information for a user.</td>
    <td style="padding:15px">{base_path}/{version}/users.profile.set?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
